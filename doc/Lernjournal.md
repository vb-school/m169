# Lernjournal


### Woche I
- Tagesziele
    - Lernumgebung einrichten (Git-Repo, Git-Client auf Win11, SSH-Key für GitLab-Konto hinterlegen, VSCode installieren)
    - Erste Überlegungen anstellen zu möglichem Projekt
    - Erstes Anschauen der Theorie (20-Infrastruktur)

- Tagesziele erreicht?
    - Git-Repo erstellt, SSH-Key generiert und zu GitLab Profil hinzuegfügt, VSCode sowie Plugins installiert, Debian (WSL) installiert
    - Theorie (20-Infrastruktur) angeschaut, meiste Informationen waren mir bereits bekannt (insbesondere aus Modul 346 sowie privater Nutzung von Docker)
    - Erste Ideen für mögliches Projekt: InterNetNews Server Inn2 in Docker-Container und daraus Image generieren? MediaWiki mit zusätzlicher MariaDB/PostgreSQL Datenbank in seperatem Container -> Docker-compose?

- Aufgetretene Probleme / Herausforderungen
    - Keinen Herausforderungen begegnet, da Tools (SSH, Git, VSCode) bereits bekannt.
    - Einzige Herausforderung: Suche nach Projektarbeit (Realisitische Ziele? Genügend Dokumentationsunterlagen zur Unterstützung vorhanden falls notwendig? etc.)
- Eingesetzte Ressourcen
    - VSCode ([Webpage MS](https://visualstudio.microsoft.com/de/free-developer-offers/))
    - Persönliches GitLab Repo ([M169](https://gitlab.com/vb-school/m169/))
    - GitLab Repo TBZ zu Modul ([M169](https://gitlab.com/ch-tbz-it/Stud/m169/))
        - [Toolumgebung](https://gitlab.com/ch-tbz-it/Stud/m169/-/tree/main/10-Toolumgebung)
        - [20-Infrastruktur](https://gitlab.com/ch-tbz-it/Stud/m169/-/tree/main/20-Infrastruktur)
    - Git for Win ([git-scm](https://git-scm.com/))
    - Windows Subsystem for Linux (WSL)
    - INN/INN2
        - https://www.eyrie.org/~eagle/software/inn/
        - https://tldp.org/HOWTO/Usenet-News-HOWTO/x248.html
        - https://www.eyrie.org/~eagle/software/inn/docs/
        - https://th-h.de/net/usenet/servers/inn/overview/

- Erledigte Übungen / Relevante Bestandteile Projekt
    - ![Screenshot git bash](/doc/media/Screenshot_2024-02-20_163602.png)

### Woche II
- Tagesziele
    - Theorie (20 & 25) anschauen und bearbeiten (theoretisches Wissen praktisch vertiefen mittels selbst gewählter/definierter Übungen)
    - Projektziel/-inhalt für weiteren Modulverlauf definieren
- Tagesziele erreicht?
    - Theorie (20 & 25) wurde angeschaut (gelesen und zur Kentniss genommen)
    - Praktische Übungen erfolgreich durchgeführt (Repetition aus Modul 346 und private Verwendung von UNIX-like Systemen): Einrichtung OpenSSH-Server auf Debian, Paketquelle für Docker festlegen, Docker-Container laufen lassen (Aufsetzen Matrix (Synapse) Server)
    - [Projektziel resp. Inhalt definiert](/doc/usenet.md)
- Aufgetretene Probleme / Herausforderungen
    - Herausforderung bei Aufsetzen von Matrix Server via Docker Containern, da Reverse Proxy (Nginx) verwendet wurde und TLS-Zertifikat via Let's Encrypt bezogen wurde, Notwendigkeit zur Verifikation von Domain entstanden (zuerst fehlgeschlagen, nach Änderungen an Konfigurationen der Docker-Containern (Docker-compose Dateien) erfolgreich)
    - Ansonsten keine weiteren Herausforderungen da Funktionsweise von OpenSSH, TLS, Reverse-Proxys sowie Cloud-Bereitstellungsmodellen bereits hinlänglich bekannt.
- Eingesetzte Ressourcen
    - [Skript zur Installation von Docker via apt](https://github.com/valentinbinotto/docker/blob/main/debian/general.sh)
    - GitLab Repo TBZ zu Modul ([M169](https://gitlab.com/ch-tbz-it/Stud/m169/))
        - [25-Sicherheit](https://gitlab.com/ch-tbz-it/Stud/m169/-/tree/main/25-Sicherheit?ref_type=heads)
        - [20-Infrastruktur](https://gitlab.com/ch-tbz-it/Stud/m169/-/tree/main/20-Infrastruktur)
- Erledigte Übungen / Relevante Bestandteile Projekt
    - Einrichtung OpenSSH-Server auf Debian
    - Paketquelle für Docker festlegen 
    - Docker-Container laufen lassen
    - Aufsetzen Matrix (Synapse) Server mittels Docker-Compose (Plus Nginx Reverse-Proxy inkl. TLS-Zertifikat von Let's Encrypt): [matrix.enatlabs.com](https://matrix.enatlabs.com)

### Woche III
- Tagesziele
    - Theorie (30) anschauen und praktische Übungen zum Vertiefen der Inhalte bearbeiten
    - Erste Vorbereitungen Infrastruktur für weitere Projektarbeit
- Tagesziele erreicht?
    - Theorie (30) wurde gelesen und zur Kentniss genommen
    - Infrastruktur für weiteren Projektverlauf wurde aufgesetzt
    - Erste Gehversuche mit Inn2
- Aufgetretene Probleme / Herausforderungen
    - Bei versuchtem Starten von Inn2 via `su news -s /bin/sh -c /usr/lib/news/bin/rc.news` wird Fehlermeldung ausgegeben `INND: No active file!`. Damit ist vermutlich die "active" File gemeint, welche definiert, welche Newsgruppen vom Server verteilt werden. Nachlesen zur Details in der entsprechenden Manpage zur active-File zur Vorbereitung zur Fehlerbehebung.
    - Fehler von apt, dass Installation von Inn2 nicht komplett abgeshclossen wurde. Liegt daran, dass zu Beginn keine gültige Konfigurationsdatei für Inn2 unter `/etc/news/inn.conf` vorhanden ist. Problem konnte druch Setzen des Hostnamens / Domain des Servers in dieser genannten Konfigurationsdatei behoben werden.
- Eingesetzte Ressourcen
    - [Newsserver How-To](https://wiki.freifunk.net/Newsserver_einrichten#Installation)
    - [Manpage zu inn.conf](https://manpages.debian.org/jessie/inn2-inews/inn.conf.5.en.html)
    - [Informationen über Inn2](https://th-h.de/net/usenet/servers/inn/)
    - [Offizielle INN Dokumentation](https://www.eyrie.org/~eagle/software/inn/docs/)
    - [FAQ zu INN2.*](https://www.eyrie.org/~eagle/faqs/inn.html)
    - [Manpage zur active-File](https://manpages.debian.org/stretch/inn2/active.5.en.html)
    - GitLab Repo TBZ zu Modul ([M169](https://gitlab.com/ch-tbz-it/Stud/m169/))
        - [30-Container](https://gitlab.com/ch-tbz-it/Stud/m169/-/tree/main/30-Container)
- Erledigte Übungen / Relevante Bestandteile Projekt
    - siehe Woche II (Aufsetzen eines Matrix (Synapse) Servers mit Docker Compose)
    - ![Screenshot Fehlermeldung Inn2 active File](/doc/media/Screenshot_2024-03-11_213538.png)
### Woche IV
- Tagesziele
    - Erste Schritte / Erstkonfiguration (rudimentäre) des INN2 News-Servers
    - Behebung Probleme bei ersten Gehversuchen mit INN2 während Woche III (no active File)
- Tagesziele erreicht?
    - Konfigurationsdatei (inn.conf) mit eigenen Details ergänzt -> Soweit für rudimentären Newsserver einsatzbereit
    - Problem aus Woche III (`INND: No active file!`) durch Erstellen einer Datei unter `/var/lib/news/active` behoben
    - Erste lokale Newsgroups erstellt
- Aufgetretene Probleme / Herausforderungen
    - Frage: Upstream-Newsserver wird benötigt um Newsfeeds zu erhalten sowie lokale Posts zu verbreiten -> Möglichkeit zwei Newsserver aufzusetzen und so Peering/Netz zu simulieren bzw. internes Netz zu erstellen -> Notwendigkeit für Tier-1 Provider entfällt
- Eingesetzte Ressourcen
    - [Newsserver How-To](https://wiki.freifunk.net/Newsserver_einrichten#Installation)
    - [Manpage zu inn.conf](https://manpages.debian.org/jessie/inn2-inews/inn.conf.5.en.html)
    - [Informationen über Inn2](https://th-h.de/net/usenet/servers/inn/)
    - [Offizielle INN Dokumentation](https://www.eyrie.org/~eagle/software/inn/docs/)
    - [FAQ zu INN2.*](https://www.eyrie.org/~eagle/faqs/inn.html)
    - [Manpage zur active-File](https://manpages.debian.org/stretch/inn2/active.5.en.html)
    - GitLab Repo TBZ zu Modul ([M169](https://gitlab.com/ch-tbz-it/Stud/m169/))
        - [30-Container](https://gitlab.com/ch-tbz-it/Stud/m169/-/tree/main/30-Container)
    - [Blogpost zur Einrichtung eines INN-Servers](http://linuxapplications.blogspot.com/2012/08/installing-inn-server.html)
    - [Peering mit einem Tier-1 Provider](https://usenetexpress.com/peering/)
- Erledigte Übungen / Relevante Bestandteile Projekt
    - ![Screenshot active File von INN2](/doc/media/Screenshot_2024-03-18_224851.png.png)
    - ![Screenshot Erstellung lokale Newsgruppe](/doc/media/Screenshot_2024-03-18_225151.png)
    - Aufsetzen von Nextcloud AIO innerhalb eines privaten Netzwerks mit zwei Reverse-Proxys davor (Caddy + Cloudflare Zero Trust Access) [Bitbucket Code für Docker-Compose](https://bitbucket.org/enatlabs/nextcloud-inton-debian-8gb-hel1-2/src/main/nextcloud/docker-compose.yml)
    - Aufsetzen von Elasticsearch via Docker-Compose (Aktuell Kibana nur via HTTP aufrufbar -> Ziel/Herausforderung Zugriff via HTTPS zu ermöglichen)

### Woche V
- Tagesziele
    - Export aller Konfigurationsdateien von INN2 (für spätere Einbindung via Volumen)
    - Erstellung zweiter Server für weitere INN2 Instanz
    - Versuch erste Verbindung von Newsreader zu Server
- Aufgetretene Probleme / Herausforderungen
    - Authentifizierung der Nutzer schlägt fehl (keine Verbindung Reader zu Server möglich)
- Eingesetzte Ressourcen
    - [Newsserver How-To](https://wiki.freifunk.net/Newsserver_einrichten#Installation)
    - [Manpage zu inn.conf](https://manpages.debian.org/jessie/inn2-inews/inn.conf.5.en.html)
    - [Informationen über Inn2](https://th-h.de/net/usenet/servers/inn/)
    - [Offizielle INN Dokumentation](https://www.eyrie.org/~eagle/software/inn/docs/)
    - [FAQ zu INN2.*](https://www.eyrie.org/~eagle/faqs/inn.html)
    - [Manpage zur active-File](https://manpages.debian.org/stretch/inn2/active.5.en.html)
    - [Blogpost zur Einrichtung eines INN-Servers](http://linuxapplications.blogspot.com/2012/08/installing-inn-server.html)
    - [Peering mit einem Tier-1 Provider](https://usenetexpress.com/peering/)
- Erledigte Übungen / Relevante Bestandteile Projekt
    - Elasticsearch Instanz von Woche IV durch Reverse Proxy ergänzt um HTTPS Verbindung zu Kibana zu ermöglichen ([Docker compose file](https://bitbucket.org/enatlabs/elastic-enatlabs-intht-ubuntu-8gb-hel1-1/src/main/elastic-enatlabs-intht/docker-compose.yml))

### Woche VI
(Krankheitsbedingt nicht am Unterricht teilgenommen)
- Tagesziele
    - Synchronisation der News-Posts zwischen 2 Nodes ermöglichen
    - Posten von News auf einem Node ermöglichen
    - Authentifizierung für Server (Node) gegenüber anderem Server (Node) ermöglichen
- Tagesziele erreicht?
    - Ja, Nachrichten können auf einem der beiden Nodes erstellt werden und werden in Echtzeit synchronisiert
- Aufgetretene Probleme / Herausforderungen
    - Synchronisation hat aufgrund fehlerhaften Pfadangabe nicht funktioniert
    - Authentifizierung der Nodes hat aufgrund falscher Reverse-DNS Einträge nicht funktioniert
    - Anmeldung über Localhost brachte keine genügenden Benutzerberechtigungen aufgrund fehlerhafter Konfiguration der Authentifizierung und Benutzerberechtigungen
- Eingesetzte Ressourcen
    - [How to set up a new NNTP peer with innfeed? (InterNetNews walkthrough)](https://www.youtube.com/watch?v=71ENBZTL0YY)
    - [Der INN im Überblick: Konzepte, Funktionsweise, Konfigurationsdateien und Logfiles](https://th-h.de/net/usenet/servers/inn/overview/)
    - [INN 2.7 Documentation](https://www.eyrie.org/~eagle/software/inn/docs-2.7/)
    - [Dukoment des ISC über INN](https://downloads.isc.org/isc/inn/extra-docs/innusenix.pdf)
    - [Manpage zur inn.conf File](https://manpages.debian.org/unstable/inn2-inews/inn.conf.5.en.html)
    - [Abschnitt zum Pullen von Nachrichten in Anleitung zum Aufsetzen eines INN-Nodes](https://wiki.freifunk.net/Newsserver_einrichten#Pull)
    - [Zur Newsfeeds File](https://www.eyrie.org/~eagle/software/inn/docs-2.7/newsfeeds.html)
    - Nerven und Freude am Ausprobieren ;)
- Erledigte Übungen / Relevante Bestandteile Projekt
    - 2 Inn2-Nodes aufgesetzt
    - Peering durchgeführt (Auth sichergestellt, Synchronisation getestet)
    - Posts in Newsgruppen erstellt, Newsgruppen erstellt, Zugriff via Telnet von einem Node auf anderen Node gestestet usw.

### Woche VII
- Tagesziele
    - Skript/Guideline zur Installation/Erstkonfiguration Inn2 Server erstellen (für spätere Verwendung bei Erstellung Docker-Image)
    - Sämtliche Konfigurationsdateien von bestehenden 2 News-Nodes exportieren (z.B. um später erste "active"-File für Docker-Image zu haben)
    - Erste Versuche Docker-Image mit Inn2 (Base: Debian) zu erstellen
- Tagesziele erreicht?
    - Skript/Grober Guideline erstellt [Git](/code/news02/news02-simplesteps)
    - Tar-Archive mit Konfigurationsdateien erstellt [Git](/code/news01-currentarch)
    - Erste Docker-Images erstellt [Example Dockerfile](/code/Dockerfile-example)
- Aufgetretene Probleme / Herausforderungen
    - Docker-Container, welche generierte Images verwenden, exiten nachdem Inn2 unter User news gestartet wurde (scheinbar für Docker nicht sichtbar, dass Prozess läuft) => Frage: Verwendung von Run as news? Oder Verwendung von supervisord etc.?
- Eingesetzte Ressourcen
    - [Dockerfile Ref.](https://docs.docker.com/reference/dockerfile/)
- Erledigte Übungen / Relevante Bestandteile Projekt
    - Kubernetes-Cluster mittels AWS EKS erstellt und erste Gehversuche mit ELK-Stack auf Kuberenetes unternommen
    - Docker-Images mit Inn2 als Apt-Packet sowie Grundkonfigurationen erstellt und Container damit gestartet

### Woche VIII
- Tagesziele
    - Docker Image Probleme von Woche VII lösen/bearbeiten
    - AWS EKS Kubernetes Cluster erstellen und Testapplikation deployen
- Tagesziele erreicht?
    - Festgestellt, dass Docker COntainer mit Image von Inn2 exited, weil Skript unter /usr/lib/news/bin/innwatch scheinbar nicht ausgeführt wird, da control-file in inn2-Konfiguration fehlt
    - AWS EKS Cluster erstellt und COmpute Nodes hinzuegfügt
    - Testapplikation mit Load Balancer (plus Service) auf EKS Cluster
- Aufgetretene Probleme / Herausforderungen
    - Weiterhin Herausforderung zum stabilen Starten des Docker Containers mit Inn2 Image
- Eingesetzte Ressourcen
    - [AWS EKS Dokumentation](https://docs.aws.amazon.com/de_de/eks/?id=docs_gateway)
- Erledigte Übungen / Relevante Bestandteile Projekt
    - AWS EKS Cluster
    - Deployment Testappplikation mit ELB und Service auf AWS EKS
    - Loganalyse plus stetiges Anpassen der Dockerfile für Inn2 Image

### Woche IX (inkl. Ferien)
- Tagesziele
    - Image auf hub.docker.com pushen
    - Elasticsearch und Nextcloud Docker-compose files aus Bitbucket verlinken
    - AWS CloudFormation Template erstellen und anwenden
- Tagesziele erreicht?
    - Image verfügbar gemacht
    - Verlinkung hergestellt
    - CF Stack erstellt
- Aufgetretene Probleme / Herausforderungen
    - Bzgl. Container mit Inn2 Newsserver exited (Kommentar dazu findet sich in Doku)
- Eingesetzte Ressourcen
    - hub.docker.com
    - AWS CloudFormation
- Erledigte Übungen / Relevante Bestandteile Projekt
    - -

### Woche X
- Tagesziele
    - Projektabschluss
        - Doku abschliessen
- Tagesziele erreicht?
    - Projektabschluss erfolgreich
- Aufgetretene Probleme / Herausforderungen
    - -
- Eingesetzte Ressourcen
    - GitLab
- Erledigte Übungen / Relevante Bestandteile Projekt
    - -
