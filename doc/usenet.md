[TOC]
# Projekt UsenetINDebDock

### Projektinhalt/-idee

Debian Docker Image wird verwendet und durch inn2 Newsserver-Packet sowie benötigte Minimalkonfiguration erweitert. Mittels Dockerfile wird daraus dann ein fertiges Docker-Image generiert.

#### Projektziele
- Inn2 Usenet/Newsserver Umgebung aufsetzen (direkt auf Host)
- Konfigurationen und Erfahrungen verwenden, um Dockerfile daraus zu erstellen
- Docker-Image aus Dockerfile sowie Konfigurationen (Minimal) für Inn2 erstellen
- Veröffentlichen des fertigen Images auf Registry
- Möglichkeiten des Betreibens von Inn2 in Docker-Containern aufzeigen

### About Inn2
INN2, vielmehr die Version 2 von INN, ist eine Newsserver-Software, wie einer der Entwickler selbst ausführt: 
> INN (InterNetNews) is a full-featured and flexible news server package, originally written by Rich Salz in 1991 as a higher performance alternative to C News.
> - https://www.eyrie.org/~eagle/software/inn/

Inn2 wird als Newsserver im sogenannten "Usenet" häufig verwendet, auch wenn das Usenet als Alternative zu Mailinglisten heute nicht mehr diesselbe Verbreitung wie vor einigen Jahrzehnten geniesst, finden Newsserver dennoch weiterhin, insbesondere in der Free Software Bewegung, Verwendung. Aufgrund dieser eher geringen Verbreitung finden sich aktuell auch keine Docker-Images für Inn2 oder Inn. Deshalb erschien es mir als interessante Herausforderung ein solches Image zu erstellen und danach auch auf einer öffentlichen Container-Registry Dritten zugänglich zu machen.

INN bzw. INN2 läuft grundsätzlich auf sämtlichen Unix und Unix-like Betriebssystemen (z.B. Linuxen (Debian, Ubuntu, Mint etc.), BSDs (FreeBSD, OpenBSD etc.)).  INN2 kann einerseits direkt aus den Quelldateien selbst kompliliert werden, aber einige Betriebssysteme bieten auch bereits fertig kompilierte Packete in ihren Packetquellen an. So habe ich auf das Inn2 Packet aus der Standart-Debian-Packetquelle zurückgegriffen:

```
root@news01:~# apt show inn2
Package: inn2
Version: 2.7.1-1+deb12u1
Priority: optional
Section: news
Maintainer: Marco d'Itri <md@linux.it>
Installed-Size: 3,724 kB
Provides: news-transport-system
Pre-Depends: inn2-inews (= 2.7.1-1+deb12u1)
Depends: libc6 (>= 2.34), libcanlock3 (>= 3.3.0), libcrypt1 (>= 1:4.1.0), libdb5.3, libpam0g (>= 0.99.7.1), libperl5.36 (>= 5.36.0), libpython3.11 (>= 3.11.0), libsasl2-2 (>= 2.1.28+dfsg), libsqlite3-0 (>= 3.20.0), libssl3 (>= 3.0.0), libsystemd0, zlib1g (>= 1:1.1.4), perl:any, perlapi-5.36.0, cron | cron-daemon, default-mta | mail-transport-agent, time, procps, libencode-perl, libmime-tools-perl, lsb-base
Suggests: gnupg1, wget, libgd-perl, libkrb5-3 (>= 1.6.dfsg.2)
Conflicts: inn, innfeed
Replaces: inn, inn2-dev, inn2-lfs, innfeed
Homepage: https://www.eyrie.org/~eagle/software/inn/
Tag: implemented-in::c, interface::daemon, network::server, protocol::nntp,
 role::program
Download-Size: 1,294 kB
APT-Manual-Installed: yes
APT-Sources: http://deb.debian.org/debian bookworm/main amd64 Packages
Description: 'InterNetNews' news server
 This package provides INN 2.x, which is a very complex news server
 daemon useful for big sites. The 'inn' package still exists for smaller
 sites which do not need the complexity of INN 2.x.
 .
 The news transport is the part of the system that stores the articles
 and the lists of which groups are available and so on, and provides
 those articles on request to users. It receives news (either posted
 locally or from a newsfeed site), files it, and passes it on to any
 downstream sites. Each article is kept for a period of time and then
 deleted (this is known as 'expiry').
 .
 By default Debian's INN will install in a fairly simple 'local-only'
 configuration.
 .
 In order to make use of the services provided by INN you'll have to
 use a user-level newsreader program such as pan. The newsreader is
 the program that fetches articles from the server and shows them to
 the user, remembering which the user has seen so that they don't get
 shown again. It also provides the posting interface for the user.
```


### Dockerfile
Als Basis für mein Image habe ich Debian verwendet. Aufgrund der Tatsache, dass ich jeweils eine Inn2-Instanz auf zwei virtuellen Maschinen in der Cloud aufgesetzt habe und das Betriebssystem dieser beiden Maschinen Debian (Bookworm, stable) ist, sowie ich persönlich Debian als äusserst stabiles, sicheres und vergleichsweise schlankes Betriebssystem kenne, lag die Wahl von Debian als Image-Basis auf der Hand. Weiter verfolgen die Debian Entwickler einen strikten Free Software Ansatz (zumindest was die stable Packetquellen betrifft), was ich persönlich ebenfalls als grossen Wert ansehe.

Folglich die erste Definition in der Dockerfile `FROM debian:stable`.
Weiter verwende ich zur späteren Identifizierung des Autor dieses Images (meiner Person) ein das Maintainer-Label: `LABEL maintainer="EETT Research - EnatLabs External Testing and Training <dockerimages.conreg.ext@eett-research.etlas.one>"`. Es sei hier angemerkt, dass ich auf diesselbe Bezeichung meiner Person wie bereits in vorhergehenden Projekten zurückgreife.

Mit `RUN apt-get update && apt-get install -y inn2 suck telnet || true && rm -rf /var/lib/apt/lists/* && mkdir /run/news && mkdir /root/va04 && chown news:news /run/news` definiere ich weiter Befehle, die während der Erstellung des Images ausgeführt werden sollen bzw. bevor das Image seine endgültige Form erhält und danach verwendet werden kann. Diese Befehle werden nicht mehr beim späteren Start der Container ausgeführt, sondern lediglich während der Erstellung des Images selbst. 

Mit `apt-get update && apt-get install -y inn2 suck telnet` aktualisiere ich die Packetquellen und installiere die Packete `inn2`, `suck` und `telnet`. `suck` dient dem initialen Download aller Newsbeiträge eines möglichen Upstream-Newsservers und `telnet` eignet sich als universelles Werkzeug auch zur Kommunikation mit Inn2 direkt über die Konsole. Angefügt daran findet sich `|| true`. `true` wird also nur ausgeführt, wenn das vorherige Kommando nicht erfolgreich oder mit Fehlern abgeschlossen wurde (`||` dient als ODER). Diese logische Verknüpfung muss ich zwingend verwenden, da bei der Installation von inn2 via apt, dieses Packet zwar vollständig installiert wird, apt aber die fehlende Initialkonfiguration von inn2 bemängelt und somit einen Fehler ausgibt. Dies führt dazu, dass die Variable `$?` nicht mit `0` sondern einer anderen Zahl belegt wird, was zwangsläufig dazu führt, dass Kommandos welche mit `&&` angehängt wurden, nicht ausgeführt werden, da mit `&&` eine logische UND Verknüpfung stattfindet. `true` führt dazu, dass diese Variable erneut mit `0` belegt wird und somit die Ausführung aller via `&&` angehängter Befehle möglich wird.

`rm -rf /var/lib/apt/lists/*` sorgt dafür, dass das der lokale Index der Packetquelleninhalte gelöscht und damit Speicherplatz frei wird. (`apt-get update` aktualisiert ja den lokalen Index der Packetquelleninhalte und belegt so einen gewissen Speicherplatz)

Mit `mkdir /run/news && mkdir /root/va04 && chown news:news /run/news` erstellen wir einerseits den Ordner `/run/news`, welcher von inn2 verlangt wird und ändern auch sogleich den Besitzer dieses Ordners. `mkdir /root/va04` erstellt einen Ordner im Home-Verzeichnis des Root-Users (Dient für zukünftige Dockerfile Versionen, in welchen temporär Ordner zum Ablegen von Initalkonfigurationsdateien benötigt werden.)

Mit `EXPOSE 119` definieren wir klar, dass später unser Docker-Container den Port 119 auf einen Port des Hostsystems binden soll (soweit durch den Nutzer mit einer expliziten Definition ermöglicht).

Die auskommentierten Volumendefinitionen (`#VOLUME ["/etc/news"]` und `#VOLUME ["/var/lib/news"]`)haben keinen Einfluss auf das schlussendliche Docker-Image. Es ist vorstellbar, dass ich in zukünftigen Versionen dieser Dockerfile Verwendung für diese Volumen finden könnte, deshalb sind diese an dieser Stelle bereits mal hinterlegt.

Für die globalen Umweltvariabeln (`ENV hostnamefqdn news02.m169.etlas.one` und ENV `initialupstream news01.m169.etlas.one`) findet sich in der aktuellen Version dieser Dockerfile keine Verwendung, es ist aber vorstellbar in späteren Versionen dieses Docker-Images einige Konfigurationsschritte über Umweltvariabeln durchzuführen.

Mit `COPY ./innconfetc /etc/news/` und `COPY ./libvarnews /var/lib/news/` kopiere ich die Inhalte der Ordner innconfetc und libvarnews in die entsprechenden Zielorder innerhalb des Containers. Die beiden Ordner enthalten wichtige Konfigurationsdateien, die Inn2 benötigt, um überhaupt als Service gestartet werden zu können. Diese Konfigurationsdateien wurden von mir bereits vorkonfiguriert (Newsserver Hostname zugewiesen, Betreiber des Newsservers angegeben etc.).

Und schlussendlich definiere ich mit `CMD ["/bin/bash", "-c", "su news -s /bin/sh -c '/usr/lib/news/bin/rc.news' && /usr/lib/news/bin/innwatch && top"]` welche Befehle beim Start des Containers aus diesem Image ausgeführt werden sollen. Mit `"/bin/bash"` definiere ich die Shell, welche für das ausführen des nachfolgenden Befehls verwendet werden soll. Mit `su news -s /bin/sh -c '/usr/lib/news/bin/rc.news'` führe ich als Nutzer `news` den Befehl zum Starten des Inn2 Servers aus. `/usr/lib/news/bin/innwatch` ruft ein Skript auf, welches grundsätzlich durch `su news -s /bin/sh -c '/usr/lib/news/bin/rc.news'` aufegrufen werden sollte, aber um sicherzustellen, dass dieses Skript korrekt aufgerufen wird und keinenfalls übergangen wird, habe ich dieses hier nochmals expilzit ausgeführt. Und schlussendlich dient `top` dazu, einen Prozess im Vordergrund laufen zu lassen, damit unser Docker Container nicht "exited". Beim Testen ohne top ist mir nämlich aufgefallen, dass Docker scheinbar nicht erkennt, dass im Hintergrund mit dem Nutzer `news` ein Prozess ausgeführt wird und in der Folge hat Docker jeweils den Container nach Aufruf der Start-Skripte von Inn2 beendet. Um dies zu verhindern ist nun `top` eingesetzt.

![Docker exit no top](/doc/media/exited-without-top.png)


```
# Debian (stable) as the Base-Image

FROM debian:stable

LABEL maintainer="EETT Research - EnatLabs External Testing and Training <dockerimages.conreg.ext@eett-research.etlas.one>"

# Install INN2, suck and telnet
RUN apt-get update && \
    apt-get install -y inn2 suck telnet || true && \
    rm -rf /var/lib/apt/lists/* && \
    mkdir /run/news && \
    mkdir /root/va04 && \
    chown news:news /run/news


# Expose port 119 for NNTP communication
EXPOSE 119

# Add option for a volume for INN2 config location (/etc/news)
#VOLUME ["/etc/news"]
#VOLUME ["/var/lib/news"]

# Default values for initial upstream newsserver and FQDN of Container
ENV hostnamefqdn news02.m169.etlas.one
ENV initialupstream news01.m169.etlas.one

# Add initial Config-Files to Image
COPY ./innconfetc /etc/news/
COPY ./libvarnews /var/lib/news/

# Start INN2 and execute top to make sure, Container stays up and running
CMD ["/bin/bash", "-c", "su news -s /bin/sh -c '/usr/lib/news/bin/rc.news' && /usr/lib/news/bin/innwatch && top"]
```


### Image erstellen

Ist die Dockerfile vollständig kann mittels `docker build -t namenstagdesimages .` Docker dazu gebracht werden im working Directory nach der Dockerfile zu suchen und den dortigen Anweisungen zu folgen.

Unser Image soll später in der Docker Registry verfügbar sein, deshalb muss ich hier zwingend den Benutzernamen sowie den Namen des Repositrys angeben:
`docker build -t eettresearch/inn2-in-dock:beta .`

Der Tag nach dem Doppelpunkt ist durch mich frei wählbar (`beta`).

### Image auf Registry pushen
Ist das Image erstellt, ist dieses nun lokal verfügbar und auch für Container einsetzbar, wir möchten das Image aber auf die Docker Regitry "pushen". Dies erfolgt mit dem Befehl `docker push eettresearch/inn2-in-dock:beta`.

Unser Image ist nun auf der Docker Registry verfügbar und kann durch Dritte genutzt werden.
[Link zum Repo](https://hub.docker.com/repository/docker/eettresearch/inn2-in-dock/general)
Via `eettresearch/inn2-in-dock:beta` kann das Image direkt über das Docker CLI verwendet werden (Exp. `docker run -it -d --name testrun-inn2 -p 119:119 eettresearch/inn2-in-dock:beta`).

![Docker Hub Repo](/doc/media/dockerhubrepo.png)
![Docker Hub Search](/doc/media/dockerhubsearchinn2.png)
![Start COntainer](/doc/media/runimag-doiwnloadregistry.png)

### Vorteile / Nachteile Inn2 in Container

Inn2 ist nun (out-of-the-box) direkt von der Container-Konsole aus nutzbar (da bei Inn2 der Localhost standartmässig immer die Berechtigung zum Zugriff auf Port 119 und den NNTP Service dahinter hat).

![Telnet localhost](/doc/media/telnet-local-insidecont.png)
![posten](/doc/media/postenviatelnet-localhost-insidecontai.png)
![Cat Post](/doc/media/cat-post-from-articledir-insidecont.png)

Für den Einsatz in einer Produktiv-Umgebung möchten wir selbstverständlich die Kommunikation der Newsserver untereinander ermgölichen. Dazu müssen wir die Datei /etc/news/readers.conf bearbeiten. Nachfolgend ein Beispiel, wie ein Eintrag zur Authentifizierung und Autorisierung eines anderen Newsservers aussehen kann:

![Readers.conf](/doc/media/readers-conf.png)

Es sei bemerkt, dass die Authentifizierung in diesem Beispiel über den Reverse-DNS-Eintrag erfolgt.

Sämtliche Konfigurationsdateien, welche ins Image eingebunden wurden und somit durch Inn2 standartmässig verwendet werden, sind im folgenden Ordner zu finden: [Git Repo Ordner](/code/prod-imagecreation)

Es sei bemerkt, dass in der aktuellen Version dieses Docker-Images keine Volumen verwendet werden und somit sämtliche Daten nur direkt innerhlab des Containers gepeichert werden. Folglich ist in der aktuellen Version dieses Images keine persistente Datenabspeicherung gewährleistet.

Inn2 innerhalb eines Docker-Containers zu betreiben kann insofern sinnvoll sein, als ein späteres Erweitern um einen Mailserver (Mail2News Gateway oder vice-versa) beispielsweise vereinfacht werden würde (In einem solchen falle müssen aber zwingend gemeinsame Volumen verwendet werden). Generell lässt sich aber sagen, dass Inn2 ein eher älteres Software-Packet ist, welches nie für das Betreiben innerhalb eines Containers konzipiert wurde. Weiter stammt Inn aus einer Zeit, in welcher das Betreiben von Diensten mit dem Microservice-Ansatz nicht Industriestandart war sowie Inn eng mit dem Usenet und der entsprechenden Gemeinschaft verbunden ist und oft auf beispielsweise FreeBSD-Systemen betrieben wird (dessen Entwickler historisch eher Kritik an Docker und dem Microservice-Ansatz geübt haben).

Inn2 in einem Docker Container zu betreiben ist dennoch ein sehr interessanter Versuch und gab mir zumindest die Möglichkeit mich in aller Tiefe mit Inn2 und Newsserver im Allgemeinen auseinanderzusetzen.

# Betreffend Docker / Container allgemein
### Example: Elasticsearch / ELK-Stack on Docker
Elasticsearch / ELK-Stack kann als Docker-Compose Cluster betrieben werden. [Die Entwickler stellen selbst eine Vorlage für eine docker-compose.yaml Datei zur Verfügung.](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html#docker-compose-file)

Die einzelnen Elatic Nodes kommunizieren untereinander mit selber signierten TLS-Zertifikaten. Kibana jedoch kommuniziert nach Aussen mit dem User über eine unverschlüsselte HTTP-Verbindung (Web Oberfläche). Elastic empfiehlt zwar die Verwendung von passenden Umweltvariabeln, um Kibana zur Nutzung von HTTPS zu bewegen, jedoch ist dieses Unterfangen aufgrund der sehr inkonsistenten Variabelnnamen eher umständlich. Deshalb habe ich mich entschieden stattdessen einen ReverseProxy im demselben Docker-Compose-Cluster laufen zu lassen und den ReverseProxy mit Kibana über HTTP und mit dem Nutzer via HTTPS kommunizieren zu lassen.

So ergibt sich folgende docker-compose.yaml File:

```
version: "2.2"

services:
  setup:
    image: docker.elastic.co/elasticsearch/elasticsearch:${STACK_VERSION}
    volumes:
      - ./certs:/usr/share/elasticsearch/config/certs
    user: "0"
    command: >
      bash -c '
        if [ x${ELASTIC_PASSWORD} == x ]; then
          echo "Set the ELASTIC_PASSWORD environment variable in the .env file";
          exit 1;
        elif [ x${KIBANA_PASSWORD} == x ]; then
          echo "Set the KIBANA_PASSWORD environment variable in the .env file";
          exit 1;
        fi;
        if [ ! -f config/certs/ca.zip ]; then
          echo "Creating CA";
          bin/elasticsearch-certutil ca --silent --pem -out config/certs/ca.zip;
          unzip config/certs/ca.zip -d config/certs;
        fi;
        if [ ! -f config/certs/certs.zip ]; then
          echo "Creating certs";
          echo -ne \
          "instances:\n"\
          "  - name: es01\n"\
          "    dns:\n"\
          "      - es01\n"\
          "      - localhost\n"\
          "    ip:\n"\
          "      - 127.0.0.1\n"\
          "  - name: es02\n"\
          "    dns:\n"\
          "      - es02\n"\
          "      - localhost\n"\
          "    ip:\n"\
          "      - 127.0.0.1\n"\
          "  - name: es03\n"\
          "    dns:\n"\
          "      - es03\n"\
          "      - localhost\n"\
          "    ip:\n"\
          "      - 127.0.0.1\n"\
          > config/certs/instances.yml;
          bin/elasticsearch-certutil cert --silent --pem -out config/certs/certs.zip --in config/certs/instances.yml --ca-cert config/certs/ca/ca.crt --ca-key config/certs/ca/ca.key;
          unzip config/certs/certs.zip -d config/certs;
        fi;
        echo "Setting file permissions"
        chown -R root:root config/certs;
        find . -type d -exec chmod 750 \{\} \;;
        find . -type f -exec chmod 640 \{\} \;;
        echo "Waiting for Elasticsearch availability";
        until curl -s --cacert config/certs/ca/ca.crt https://es01:9200 | grep -q "missing authentication credentials"; do sleep 30; done;
        echo "Setting kibana_system password";
        until curl -s -X POST --cacert config/certs/ca/ca.crt -u "elastic:${ELASTIC_PASSWORD}" -H "Content-Type: application/json" https://es01:9200/_security/user/kibana_system/_password -d "{\"password\":\"${KIBANA_PASSWORD}\"}" | grep -q "^{}"; do sleep 10; done;
        echo "All done!";
      '
    healthcheck:
      test: ["CMD-SHELL", "[ -f config/certs/es01/es01.crt ]"]
      interval: 1s
      timeout: 5s
      retries: 120

  es01:
    depends_on:
      setup:
        condition: service_healthy
    image: docker.elastic.co/elasticsearch/elasticsearch:${STACK_VERSION}
    volumes:
      - ./certs:/usr/share/elasticsearch/config/certs
      - ./esdata01:/usr/share/elasticsearch/data
    ports:
      - ${ES_PORT}:9200
    environment:
      - node.name=es01
      - cluster.name=${CLUSTER_NAME}
      - cluster.initial_master_nodes=es01,es02,es03
      - discovery.seed_hosts=es02,es03
      - ELASTIC_PASSWORD=${ELASTIC_PASSWORD}
      - bootstrap.memory_lock=true
      - xpack.security.enabled=true
      - xpack.security.http.ssl.enabled=true
      - xpack.security.http.ssl.key=certs/es01/es01.key
      - xpack.security.http.ssl.certificate=certs/es01/es01.crt
      - xpack.security.http.ssl.certificate_authorities=certs/ca/ca.crt
      - xpack.security.transport.ssl.enabled=true
      - xpack.security.transport.ssl.key=certs/es01/es01.key
      - xpack.security.transport.ssl.certificate=certs/es01/es01.crt
      - xpack.security.transport.ssl.certificate_authorities=certs/ca/ca.crt
      - xpack.security.transport.ssl.verification_mode=certificate
      - xpack.license.self_generated.type=${LICENSE}
    mem_limit: ${MEM_LIMIT}
    ulimits:
      memlock:
        soft: -1
        hard: -1
    healthcheck:
      test:
        [
          "CMD-SHELL",
          "curl -s --cacert config/certs/ca/ca.crt https://localhost:9200 | grep -q 'missing authentication credentials'",
        ]
      interval: 10s
      timeout: 10s
      retries: 120

  es02:
    depends_on:
      - es01
    image: docker.elastic.co/elasticsearch/elasticsearch:${STACK_VERSION}
    volumes:
      - ./certs:/usr/share/elasticsearch/config/certs
      - ./esdata02:/usr/share/elasticsearch/data
    environment:
      - node.name=es02
      - cluster.name=${CLUSTER_NAME}
      - cluster.initial_master_nodes=es01,es02,es03
      - discovery.seed_hosts=es01,es03
      - bootstrap.memory_lock=true
      - xpack.security.enabled=true
      - xpack.security.http.ssl.enabled=true
      - xpack.security.http.ssl.key=certs/es02/es02.key
      - xpack.security.http.ssl.certificate=certs/es02/es02.crt
      - xpack.security.http.ssl.certificate_authorities=certs/ca/ca.crt
      - xpack.security.transport.ssl.enabled=true
      - xpack.security.transport.ssl.key=certs/es02/es02.key
      - xpack.security.transport.ssl.certificate=certs/es02/es02.crt
      - xpack.security.transport.ssl.certificate_authorities=certs/ca/ca.crt
      - xpack.security.transport.ssl.verification_mode=certificate
      - xpack.license.self_generated.type=${LICENSE}
    mem_limit: ${MEM_LIMIT}
    ulimits:
      memlock:
        soft: -1
        hard: -1
    healthcheck:
      test:
        [
          "CMD-SHELL",
          "curl -s --cacert config/certs/ca/ca.crt https://localhost:9200 | grep -q 'missing authentication credentials'",
        ]
      interval: 10s
      timeout: 10s
      retries: 120

  es03:
    depends_on:
      - es02
    image: docker.elastic.co/elasticsearch/elasticsearch:${STACK_VERSION}
    volumes:
      - ./certs:/usr/share/elasticsearch/config/certs
      - ./esdata03:/usr/share/elasticsearch/data
    environment:
      - node.name=es03
      - cluster.name=${CLUSTER_NAME}
      - cluster.initial_master_nodes=es01,es02,es03
      - discovery.seed_hosts=es01,es02
      - bootstrap.memory_lock=true
      - xpack.security.enabled=true
      - xpack.security.http.ssl.enabled=true
      - xpack.security.http.ssl.key=certs/es03/es03.key
      - xpack.security.http.ssl.certificate=certs/es03/es03.crt
      - xpack.security.http.ssl.certificate_authorities=certs/ca/ca.crt
      - xpack.security.transport.ssl.enabled=true
      - xpack.security.transport.ssl.key=certs/es03/es03.key
      - xpack.security.transport.ssl.certificate=certs/es03/es03.crt
      - xpack.security.transport.ssl.certificate_authorities=certs/ca/ca.crt
      - xpack.security.transport.ssl.verification_mode=certificate
      - xpack.license.self_generated.type=${LICENSE}
    mem_limit: ${MEM_LIMIT}
    ulimits:
      memlock:
        soft: -1
        hard: -1
    healthcheck:
      test:
        [
          "CMD-SHELL",
          "curl -s --cacert config/certs/ca/ca.crt https://localhost:9200 | grep -q 'missing authentication credentials'",
        ]
      interval: 10s
      timeout: 10s
      retries: 120

  kibana:
    depends_on:
      es01:
        condition: service_healthy
      es02:
        condition: service_healthy
      es03:
        condition: service_healthy
    image: docker.elastic.co/kibana/kibana:${STACK_VERSION}
    volumes:
      - ./certs:/usr/share/kibana/config/certs
      - ./kibanadata:/usr/share/kibana/data
    ports:
      - ${KIBANA_PORT}:5601
    environment:
      - SERVERNAME=kibana
      - ELASTICSEARCH_HOSTS=https://es01:9200
      - ELASTICSEARCH_USERNAME=kibana_system
      - ELASTICSEARCH_PASSWORD=${KIBANA_PASSWORD}
      - ELASTICSEARCH_SSL_CERTIFICATEAUTHORITIES=config/certs/ca/ca.crt
      - SERVER_PUBLICBASEURL=https://elastic-intht.enatlabs.com
    mem_limit: ${MEM_LIMIT}
    healthcheck:
      test:
        [
          "CMD-SHELL",
          "curl -s -I http://localhost:5601 | grep -q 'HTTP/1.1 302 Found'",
        ]
      interval: 10s
      timeout: 10s
      retries: 120

  caddy:
    image: caddy:alpine
    container_name: proxy
    restart: always
    volumes:
      - ./reverseproxy/Caddyfile:/etc/caddy/Caddyfile
      - ./reverseproxy/certs:/certs
      - ./reverseproxy/config:/config
      - ./reverseproxy/data:/data
      - ./reverseproxy/sites:/srv
    ports:
      - "443:443"
```
In der Konfigurationsdatei des ReverseProxys (Caddyfile) werden die zu verwendenden TLS-Zertifikate definiert, sowie festgelegt, dass die Anfragen an `https://elastic-intht.enatlabs.com` an den Host `elastic-enatlabs-com_kibana_1` weitergeleitet werden. Hier kann ein Hostname verwendet werden, da innerhalb desselben Docker Netzwerks die Container untereinander über die Containernamen kommunizieren können. Theoretisch könnten wir hier nun bei der Definition des Containers für Kibana die Defintion des Port-Bindings entfernen, im Falle von Elastic möchten wir aber unter Umständen direkt mit Kibana kommunizieren (von ausserhalb des Hosts)(exp. Im Falle eines ReverseProxys auf einem anderen Host im Netzwerk (Subnet VPC, Cloudflare Tunnel auf anderer EC2-instanz)).

```
http://elastic-intht.enatlabs.com {
	redir https://{host}{uri}
}

https://elastic-intht.enatlabs.com {
	tls /certs/enatlabs-elastic.cert.pem /certs/enatlabs-elastic.key.pem
	header Strict-Transport-Security max-age=31536000;
	reverse_proxy elastic-enatlabs-com_kibana_1:5601
}
```


In der .env Datei (welche Variabeln für die docker-compose.yaml File enthält) finden wir auch die Möglichkeit die Nutzung des Arbeitsspeichers durch einzelne Container zu beschränken. Dasselbe ist auch mit der CPU-Runtime möglich (exp. beim Starten eines Containers via Docker run: `docker run [..] -c 1024`).

Alle weiteren Dateien (.env etc.) finden sich in nachfolgendem Bitbucket Repo:
[Bitbucket Elastic](https://bitbucket.org/enatlabs/elastic-enatlabs-intht-ubuntu-8gb-hel1-1/src/main/elastic-enatlabs-intht/)


### Example: Nextcloud Docker-Compose
Nextcloud als Alternative zu Cloud für Privatpersonen von grossen Anbietern wie Microsoft oder Dropbox, lässt sich ebenfalls in Docker-Containern deployen. Die durch die Entwickler veröffentlichten "docker run"-Befehle lassen sich in eine docker-compose.yaml File umschreiben:

```
version: "3.3"

services:
  caddy:
    image: caddy:alpine
    container_name: proxy
    restart: always
    volumes:
      - ./Caddyfile:/etc/caddy/Caddyfile
      - ./certs:/certs
      - ./config:/config
      - ./data:/data
      - ./sites:/srv
    ports:
      - "443:443"
    networks:
      caddy:
        ipv4_address: 172.22.0.2
    extra_hosts: ["host.docker.internal:host-gateway"]

  nextcloud:
    image: nextcloud/all-in-one:latest
    restart: always
    container_name: nextcloud-aio-mastercontainer
    ports:
      - "8080:8080"
    environment:
      - APACHE_PORT=11001
      - APACHE_IP_BINDING=0.0.0.0
      - SKIP_DOMAIN_VALIDATION=true
    volumes:
      - nextcloud_aio_mastercontainer:/mnt/docker-aio-config
      - /var/run/docker.sock:/var/run/docker.sock:ro
    depends_on:
      - caddy

volumes:
  nextcloud_aio_mastercontainer:
    name: nextcloud_aio_mastercontainer

networks:
  caddy:
    external: true
    
```

Die Konfigurationsdateien finden sich in meinem persönlichen Bitbucket Repo zu diesem Thema:
[Bitbucket zu Nextcloud](https://bitbucket.org/enatlabs/nextcloud-inton-debian-8gb-hel1-2/src/main/nextcloud/)

Bei dieser docker-compose.yaml Datei fällt uns auf, dass wir ein zusätzliches Netzwerk für Caddy verwenden. Da wir dieses Netzwerk bereits im Vorhinein mit `docker network create caddy` erstellt haben, definieren wir in unserer docker-compose.yaml Datei dieses Netzwerk als "external". Das gibt Docker die Anweisungen dieses Netzwerk nicht neu zu erstellen, sondern auf ein bestehendes Netzwerk mit diesem Namen zurückzugreifen.

Da sich Caddy nun in einem anderen Netzwerk wie unser Nextcloud Mastercontainer befindet, funktioniert nun die Kommunikation via Hostnamen (DNS) nicht mehr. Da wir diese Funktionalität aber nicht missen möchten, fügen wir Caddy noch eine zusätzliche Definition hinzu:`extra_hosts: ["host.docker.internal:host-gateway"]`. Damit können wir mit dem Host (auf welchem die Docker-Umgebung läuft) kommunizieren. Der Nachteil ist jedoch, dass wir nun die Ports von Nextcloud welche wir von Caddy aus erreichen möchten, auf entsprechende Host-Ports binden müssen (Theoretisch lässt sich dies jedoch mit APACHE_IP_BINDING=127.0.0.0 auf den localhost beschränken).


### Docker / Container in der Cloud (AWS)
In Kombination mit einer Public Cloud Umgebung (exp. AWS) lässt sich mit Docker (allgemein Containern) unter bestimmten Umständen äusserst kostensparend arbeiten, da beim sogenannten "Serverless"-Prinzip lediglich die Container/Images durch den Nutzer bereitgestellt und verwaltet werden müssen, die darunterliegende Server-Infrastruktur jedoch vollständig durch den Anbieter verwaltet wird. Damit entfällt auch das teilweise sehr zeitaufwändige Aktualisieren der Betriebssysteme der Hosts auf welchen die Container ausgeführt werden etc.
Auch wird die Nutzung von Services wie z.B. Elastic Load Balancing vereinfacht, da diese Funktionen häufig bereits in entsprechenden Serverless-Services integriert sind.

Ein Beispiel für einen Service zum Betreiben von Containern in der Cloud ist Amazon ECS (Amazon Elastic Container Service) oder AWS Fargate (Serverless Container betreiben).

#### Amazon ECS
Nachfolgend ein Beispiel, wie Docker-Container mit Amazon ECS genutzt werden können. Wir verwenden AWS Fargate da wir hier nutzungsabhängig und somit nur dann bezahlen müssen, wenn tatsächlich Workloads ausgeführt werden:

![cluster](/doc/media/create-ecs-cluster.png)
![overview ecs](/doc/media/cluster-overview.png)

Nachdem wir ein Cluster und somit auch Rechenleistung zur Verfügung haben, müssen wir eine Aufgabendefinition erstellen, in welcher wir das zu verwendene Docker-Image angeben (Grüsse an Luca Suter (Ersteller des Images lsgitea)) sowie Parameter wie Archidektur auf welcher die Container ausgeführt werden sollen, maximaler Arbeitsspeicher für die Task oder Metriken, welche an CloudWatch zur Überwachung weitergereicht werden sollen.
![task01](/doc/media/create-taskdefintion-01.png)
![task02](/doc/media/create-taskdefi-02.png)
![task03](/doc/media/create-taskdefi-03.png)
![taskover](/doc/media/taskdefi-overview-compl.png)

Haben wir eine Aufgabendefinition erstellen wir einen Service auf unserem ECS-Cluster. Bei der Erstellung des Services geben wir unsere Aufgabendefinition von vorhin an sowie definieren einige die Subnetze eines VPCs, um später Zugriff auf unsere Container zu erhalten. Auch haben wir gleich hier die Möglichkeit einen Load Balancer für unseren Service zu konfigurieren.

In meinem Falle benötige ich lediglich eine öffentliche IPv4-/IPv6-Adresse, denn ich möchte lediglich die Verfügbarkeit meines Containers testen und diesen nicht produktiv verwenden.
![service01](/doc/media/aws-cretae-service-001.png)
![ser02](/doc/media/create-service-02.png)
![serv03](/doc/media/create-service-03.png)
![serv04](/doc/media/ctea-service-04.png)
![servsho](/doc/media/service-short-overview.png)

Wir können direkt über die AWS ECS Konsole die Logs / Mitteilung unseres Services, also aller Container dieses Services, einsehen.
![logs](/doc/media/containerlogs.png)

Den nachfolgenden Angaben entnehmen wir die öffentliche IPv4-Adresse um auf unseren Container (auf dessen Webinhalte) zuzugreifen.
![nettask](/doc/media/network-taskdefin.png)

Wie nachfolgend aufgezeigt ist unser Container erreichbar.
![iptask](/doc/media/curl-ip-ecs-task.png)
![webpage](/doc/media/overview-gittea-website-http.png)

Theoretisch liesse sich hier eine Datenbank von AWS (exp. AWS RDS) verwenden.

In CloudWatch haben wir auch in die Metriken und Ereignisse von Amazon ECS Einsicht. Hier liessen sich selbstredend auch Alarmmitteilungen etc. konfigurieren, um uns bei allfälligen Speicher-/CPU-/XYZ-Engpässen zu informieren oder es liessen sich gar Abläufe konfigurieren (exp. Mehr Container bereitstellen, mehr Arbeitsspeicher für Service bereitstellen etc.). Selbstredend liessen sich Alarmierungen auch mit AWS SNS verbinden (für Benachrichtungen via Mail, SMS, Slack, Teams etc.).
![ecscloudwatch](/doc/media/cloudwatch-ecs.png)



### Kritik des Microservice-Ansatzes
Grundsätzlich bietet der Microservice-Ansatz (also das Aufsplitten einer grossen monolithischen Architektur in viele kleine Microservices) grosse Chancen besonders hinsichtlich Hochverfügbarkeit (HA):

Das Auftrennen von Aufgaben auf verschiedene nur lose miteinander verbundene Infrastrukturteile bietet so zum Beispiel den Vorteil, dass beim Auftreten eines Fehlers im einen Teil (einem Mailservice beispielsweise), ein anderer Teil (beispielsweise ein Webservice) nicht zwangsläufig ebenfalls unverfügbar wird. Auch haben wir hier die Chance schnell und einfach zu skalieren (horizontale Skalierung) indem wir zusätzliche Teile zur Infrastruktur hinzufügen (mehr Container, Nodes etc.).
Insbesondere im Zusammenhang mit asynchronen Services zeigt sich hier das Ziel bei der Microservice-Architektur, einzelne kleine Services möglichst eigenständig und unabhängig voneinander zu machen.

Die Verwendung von Microservices bringt aber auch Nachteile und Risiken mit sich:
So besteht paradoxerweise das Risiko, dass durch die Verwendung von unzähligen kleinen Komponenten die komplette Infrastruktur sehr unübersichtlich wird. Auch müssen Services teilweise derart eng zusammenarbeiten, dass zusätzliche Latenz bei der Komunikation über Containergrenzen hinweg nicht tolerierbar ist. Wird die Microservice-Infrastruktur in der Cloud betrieben, muss weiter noch mit Kosten für die Kommunikation der Container untereinander gerechnet werden, was teilweise finanziell im Vergleich zu einer monolithischen Architektur sehr unattraktiv erscheint.


#### Kritik Serverless Cloud
Insbesondere im Zusammenhang mit Microservices wird häufig von "Serverless" gesprochen. Damit wird beabsichtigt sich bei der zukünftigen Konstruktion voll und ganz auf die Abstraktionsebene der Container-Konfiguration beschränkt. Dies birgt aber das grosse Risiko wertvolles Wissen zur Wartung, Bereitstellung, Administration von darunterliegender Infrastruktur (Netzwerke, Betriebssysteme etc.) verloren geht und dieses durch proritäres Wissen zur Bedingung der Infrastruktur einzelner kommerzieller Cloud-Betreiber ersetzt wird.


# IaC
Infrastructure as Code (IaC) vereinfacht und vereinheitlicht die Bereitstellung von Infrastruktur. Mit IaC können mittels Ressourcendefinitionen beispielsweise virtuelle Maschinen vorkonfiguriert werden.

Insbesondere im Zeitalter des Cloud Computing bietet IaC grosse Möglichkeiten. So kann beispielsweise bei AWS via CloudFormation innert Minuten ein komplettes VPC inkl. Subnetzen und Internet Gateways erstellt und bereitgestellt werden. 

IaC bietet neben der Vereinheitlichung auch den Vorteil der Nachvollziehbarkeit: Mittels CI/CD-Pipelines könnten so Änderung an einer Ressourcendefinition in einem Git-Repo beispielsweise automatisiert an CloudFormation weitergereicht und dort aktiv werden. Dabei kann von den Vorzügen eines Versionsverwaltungssystems wie Git profitiert werden. Bei Laufzeitfehlern während des Deployens von Ressourcen gestaltet sich auch ein allfälliger vollständiger Rollback einfacher.

### AWS CloudFormation
_Ich verweise zusätzlich zu den nachfolgenden Inhalten auf meine CloudFormation Templates aus dem Modul 346: [EC2](https://gitlab.com/vb-school/modul_346/-/blob/main/Weitere_Ressourcen/Skripte/int-nextcloud-ec2-launch.yaml?ref_type=heads) und [VPC](https://gitlab.com/vb-school/modul_346/-/blob/main/Weitere_Ressourcen/Skripte/eu-central-1-vpc.yaml?ref_type=heads)_


```
Resources:
  EC2DebianApache2Clone:
    Type: AWS::EC2::Instance
    Properties:
      Tags:
        - Key: Name
          Value: mhovu-org.clonemirror.m169.eett-research
      ImageId: ami-058bd2d568351da34
      InstanceInitiatedShutdownBehavior: stop
      InstanceType: t2.micro
      KeyName: nontrust.m169.eett-research
      Monitoring: true
      AvailabilityZone: us-east-1a
      UserData: !Base64
        Fn::Sub: |
          #!/bin/bash
          apt update
          apt install apache2 curl wget -y
          cd /var/www/html
          rm index.html
          wget http://valentin-43-44.org/index.html
          systemctl restart apache2
      SecurityGroupIds: 
        - sg-0c4ad0134ca17e537
      SubnetId: subnet-095342d2a9b432502
```
Dieses CloudFormation Template erstellt eine EC2-Instanz des Typs t2.micro im angegebenen Subnetz, fügt den definierten SSH-Key sowie die angegebenen Security Groups hinzu und führt nach Start der Instanz einige Kommandos aus, welche unter anderem einen Apache2 Server installieren.

Selbstredend lässt sich via CloudFormation-templates äusserst grosse und komplexe Infrastrukturen aufbauen. AWS selbst verwendet für einige Services im Hintergrund CloudFormation: So wird bei der Erstellung eines Amazon ECS Clusters beispielsweise ein CloudFormation Stack erstellt und dieser ausgeführt. Oder die Verwaltung mehrerer AWS-Root-Accounts bzw. Organisationen via Control Tower erfolgt im Hintergrund ebenfalls zum Teil via CloudFormation.

![Create CF Stack](/doc/media/cloudformation-creatstack-01.png)
![Create CF Stack 02](/doc/media/create-stack-02.png)
![Status CF](/doc/media/checkstatus-cloudform-createstack.png)
![CF complete](/doc/media/executestack-complete.png)
![EC2 VPC](/doc/media/ec2-vpc.png)
![Webpage curl](/doc/media/webpage-curl.png)